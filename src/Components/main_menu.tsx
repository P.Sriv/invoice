import React, { useState } from "react"

export interface Order{
    name:String,
    price: Number,
    qty: Number
}
export function Menu(){
    // interface User{
    //     name: String,
    //     orderList : [Order]

    // }
  
    const [order, setOrder] = useState<Order[]>([
       
      ]);

    const addColumns = (e:React.MouseEvent<any>)=>{
        setOrder([...order, { name: '', price: 0, qty: 0 }]);
    }
    const updateOrder = (index:number,e:React.ChangeEvent<any>)=>{
        
        const {name,value} = e.target
        const newdata = [...order]
        newdata[index] = {...newdata[index],[name]:value}
        setOrder(newdata)
    }

    return (
    <>
    <div className="overflow-x-auto">
  <table className="table">
    {/* head */}
    <thead>
      <tr>
        <th>Order No</th>
        <th>Name</th>
        <th>Price</th>
        <th>Quantity</th>
      </tr>
    </thead>
    <tbody>
    
      {order.map((data,index)=>(
         <tr className="hover" key={index}>
         <th>{index+1}</th>
         <td><input name="name" value={data.name as string} onChange={e=>updateOrder(index,e)}></input></td>
         <td>{data.price as number}</td>
         <td>{data.qty as number}</td>
         </tr>
      ))}
      
     
    </tbody>
  </table>
  <div className=" flex justify-end mx-4">
    <button className="" onClick={e=>addColumns(e)}>
        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" strokeWidth={1.5} stroke="currentColor" className="w-12 h-12">
        <path strokeLinecap="round" strokeLinejoin="round" d="M12 9v6m3-3H9m12 0a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z" />
        </svg>
    </button>
  </div>
 

</div>
    <button className="btn btn-primary">Primary</button>

    </>
    )
}