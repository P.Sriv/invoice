import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import {Menu} from './Components/main_menu'
function App() {
  // const [count, setCount] = useState(0)

  return (
    <>
      <h1 className="text-3xl font-bold underline">
      Hello world from App.tsx !
    </h1>
    <Menu/>
    </>
  )
}

export default App
